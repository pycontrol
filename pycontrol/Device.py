import PyTango, time

class Device(PyTango.DeviceProxy):
	_wait_enable = False;
	_wait_states = [PyTango.DevState.STANDBY]

	def __init__(self, name, wait_enable=False, wait_states=[PyTango.DevState.STANDBY]):
		PyTango.DeviceProxy.__init__(self, name)
		self.wait_enable(wait_enable)
		self.wait_states(wait_states)

	def __setattr__(self, name, value):
		if name in self.__dict__:
			self.__dict__[name] = value
		else:
			try:
				PyTango.DeviceProxy.__setattr__(self, name, value)
				if self._wait_enable == True:
					while (self.state() not in self._wait_states and self.state() != PyTango.DevState.ALARM):
						pass
			except AttributeError:
				self.__dict__[name] = value
			except KeyboardInterrupt:
				raise

	def wait_enable(self, enable):
		"""
		Allow or not to wait for the wait_states when writting on an attribute.
		enable: 0 or 1
		"""
		self._wait_enable = enable

	def wait_states(self, states):
		"""
		Set the state to wait when writting on a device attribute
		states: a PyTango.DevState or a list(PyTango.DevState)
		"""
		if isinstance(states, list):
			self._wait_states = states
		else:
			self._wait_states = [states]


	def __repr__(self):
		res = PyTango.DeviceProxy.__repr__(self) + ' "' + self.name() + '"'
		res += '\n STATE : ' + self.state().name
		res += '\n'
		res += '\n STATUS :'
		for i, l in enumerate(self.status().split('\n')):
			if len(l):
				if i:
					res += '\n         '
				res += l
		if not res.endswith('\n'):
			res += '\n'
		res += '\n ATTRIBUTES : '
		for i, c in enumerate(self.attribute_list_query()):
			if i:
				res += '\n              '
			try:
				res += c.name + " " + self.__getattr__(c.name).__repr__()
			except:
				res += c.name
		res += '\n'
		res += '\n COMMANDS : '
		for i, c in enumerate(self.command_list_query()):
			if i:
				res += '\n            '
			res += c.cmd_name
		return res



class SimpleScan(Device):
	def __init__(self, name):
		Device.__init__(self, name, True, PyTango.DevState.ON)
	
class ScanServer(Device):
	def __init__(self, name):
		Device.__init__(self, name, True, PyTango.DevState.ON)

class Rontec(Device):
	def __init__(self, name):
		Device.__init__(self, name, True, PyTango.DevState.OFF)
		
	def __call__(self, dt):
		self.integraTionTime = dt
		self.start()

class Lakeshore_340(Device):
	def __init__(self, name):
		Device.__init__(self, name, True, PyTango.DevState.STANDBY)

class Attenuator(Device):
	def __init__(self, name):
		Device.__init__(self, name, False, PyTango.DevState.ON)
		
class NI6602(Device):
	def __init__(self, name):
		Device.__init__(self, name, True)

	def __call__(self, value, counter = None):
		continuous = self.continuous
		it = self.integrationTime
		self.continuous = 0
		self.integrationTime = value
		self.start()
		while self.state() != PyTango.DevState.STANDBY:
			pass
		if counter:
			print getattr(self, "counter%d" % counter)
		self.continuous = continuous
		self.integrationTime = it
		if continuous:
			self.Start()

class Motor(Device):
	def __init__(self, name):
		Device.__init__(self, name, True, PyTango.DevState.STANDBY)

	def __setattr__(self, name, value):
		if name.lower() == "position":
			try:
				Device.__setattr__(self, name, value)
			except KeyboardInterrupt:
				self.Stop()
				print "Current position %f" % self.position
		else:
			Device.__setattr__(self, name, value)

class XPSAxis(Motor):
	def __init__(self, name):
		Motor.__init__(self, name)

class GalilAxis(Motor):
	def __init__(self, name):
		Motor.__init__(self, name)

class PseudoAxis(Motor):
	def __init__(self, name):
		Motor.__init__(self, name)

class ImgGrabber(Device):
	def __init__(self, name):
		Device.__init__(self, name, True, [PyTango.DevState.OPEN, PyTango.DevState.RUNNING])
