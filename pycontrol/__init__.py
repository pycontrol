import PyTango, Device, completion
from macros import *

def devices():
    """
    create a dictionnay with all the devices
    """
    _database = PyTango.DeviceProxy('sys/database/dbds1')
    classes = _database.DbGetClassList("*")
    devices = dict()
    for klass in classes:
        for device_name in _database.DbGetExportdDeviceListForClass(klass):
            try:
                alias = _database.DbGetDeviceAlias(device_name)
                try:
                    devices[alias] = eval("Device.%s(device_name)" % klass)
                except:
                    devices[alias] = Device.Device(device_name, True)
            except PyTango.DevFailed:
                pass
    return devices

def clean():
    for key, value in __builtins__.items():
        if isinstance(value, Device.Device):
            del __builtins__[key]
     
def reconfigure():
    clean()
    dev = devices()
    for alias in dev:
        __builtins__[alias] = dev[alias]
        if _in_ipython:
            ip = IPython.ipapi.get()
            ip.set_hook('complete_command', completion.tango_completers, re_key = '.*' + alias + '\.')


_in_ipython = False
try:
    import IPython.ipapi
    ip = IPython.ipapi.get()
    if ip:
        _in_ipython = True
except:
    pass

reconfigure()
