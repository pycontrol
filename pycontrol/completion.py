#import IPython.ipapi
#import os
#import sys
#import Device
#import PyTango
#import time
#import pylab
from rlcompleter import *
import PyTango

"""
def add_devices_class(device_class):
    ip = IPython.ipapi.get()
    database = PyTango.DeviceProxy('sys/database/dbds1')
    l = []
    for device_name in database.DbGetExportdDeviceListForClass(device_class):
        try:
            device_alias_name = database.DbGetDeviceAlias(device_name)
            ip.set_hook('complete_command', tango_completers, re_key='.*' + device_alias_name + '\.')
            try:
                globals()[device_alias_name] = eval("Device.%s(device_name)" % device_class)
                print "cool %s" % device_class
            except:
                globals()[device_alias_name] = Device.Device(device_name, True)
            l += [device_alias_name]
        except PyTango.DevFailed:
            #print "Device without alias " + device_name
            pass
    globals()[device_class] = l
"""

class TgCompleter(Completer):
    def __init__(self, namespace = None):
        Completer.__init__(self, namespace)
        
    def complete(self, text, state):
        return Completer.complete(self, text, state)

    def attr_matches(self, text):
        """Compute matches when text contains a dot.

        Assuming the text is of the form NAME.NAME....[NAME], and is
        evaluatable in self.namespace, it will be evaluated and its attributes
        (as revealed by dir()) are used as possible completions.  (For class
        instances, class members are also considered.)

        WARNING: this can still invoke arbitrary C code, if an object
        with a __getattr__ hook is evaluated.

        """
        import re
        m = re.match(r"(\w+(\.\w+)*)\.(\w*)", text)
        if not m:
            return
        expr, attr = m.group(1, 3)
        object = eval(expr, self.namespace)
        if isinstance(object, PyTango.DeviceProxy):
            words = []
            words = words + get_tango_attributes(object)
            words = words + get_tango_commands(object)
        elif hasattr(object,'__class__'):
            words = dir(object)
            words.append('__class__')
            words = words + get_class_members(object.__class__)
        matches = []
        n = len(attr)
        for word in words:
            if word[:n] == attr and word != "__builtins__":
                matches.append("%s.%s" % (expr, word))
        return matches

def get_tango_attributes(device):
    return [c.name for c in device.attribute_list_query()]

def get_tango_commands(device):
      return [c.cmd_name + '()' for c in device.command_list_query()]

def get_class_members(klass):
    ret = dir(klass)
    if hasattr(klass,'__bases__'):
        for base in klass.__bases__:
            ret = ret + get_class_members(base)
    return ret

def tango_completers(self, event):
    device_name = event.symbol.split('.')[0]
    device = eval(device_name)
    words = []
    if isinstance(device, PyTango.DeviceProxy):
        words += [device_name + '.' + c for c in get_tango_attributes(device)]
        words += [device_name + '.' + c for c in get_tango_commands(device)]
    return words

try:
    import readline
except ImportError:
    pass
else:
    readline.set_completer(TgCompleter().complete)
    readline.parse_and_bind("tab: complete")

