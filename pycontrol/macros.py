import IPython.ipapi
import os
import sys
import Device
import PyTango
import numpy
import time
import pylab

def tango_completers(self, event):
    device_name = event.symbol.split('.')[0]
    deviceproxy = eval(device_name)
    commands = [device_name + '.' + c.name for c in deviceproxy.attribute_list_query()]
    commands += [device_name + '.' + c.cmd_name + '()' for c in deviceproxy.command_list_query()]
    return commands

# helper command
def atk(*kw):
    try:
        name = kw[0].name()
    except:
        name = kw[0]
    os.system('atkpanel %s &' % name)

def charleston(device):
	os.system('charleston %s advanced &' % device.name())

"""
def restart(device):
    info = device.info()
    id = info.server_id
    starter_name = 'tango/admin/%s' % info.server_host.split('.')[0]
    starter = PyTango.DeviceProxy(starter_name)
    if id in starter.RunningServers:
        print "Restarting %s" % id,
        sys.stdout.flush()
        starter.DevStop(id)
        while id in starter.RunningServers:
             pass
        print " Stopped...",
        sys.stdout.flush()
        while starter.state() == PyTango.DevState.MOVING:
            pass
        if id in starter.StoppedServers:
            starter.DevStart(id)
            while id in starter.StoppedServers:
                 pass
            while starter.state() == PyTango.DevState.MOVING:
                pass
            print "Started DONE"
"""

class ScanHelper:
    def __init__(self, simplescan):
        self._simplescan = simplescan
        self.scanserver = Device.Device(self._simplescan.CurrentScanServer)

    def load(self, name):
        """
        load a salsa configuration into the scan server
        """
        self._simplescan.Init()
        # try to find the right configuration
        configuration = ''
        #exact match
        if name in self._simplescan.configurationList:
            configuration = name
        else: # then give a list of configuration containing the name
            l = [c for c in self._simplescan.configurationList if name in c]
            if len(l) != 1:
                raise PyTango.DevFailed(l)
            else:
                configuration = l[0]

        if self._simplescan.state() in [PyTango.DevState.ON, PyTango.DevState.ALARM]:
            self._simplescan.configuration = configuration
            self.scanserver = Device.Device(self._simplescan.CurrentScanServer)
            self._simplescan.UploadConfigToScanServer()
            
            # get the number of actuators per scan dimension
            if len(self.scanserver.actuators):
                self.dims = len(self.scanserver.actuators), 
            if len(self.scanserver.actuators2):
                self.dims += len(self.scanserver.actuators2),
        else:
                raise PyTango.DevFailed("Scan already in progress ! try later")

    def start(self):
        """
        start the current loaded scanserver configuration
        """
        if self.scanserver.state() in [PyTango.DevState.ON, PyTango.DevState.ALARM]:
            self.scanserver.start()
            while self.scanserver.state() not in [PyTango.DevState.ON, PyTango.DevState.ALARM]:
                time.sleep(.1)
        else:
            print("Scan already in progress !")

    def __repr__(self):
        res = """Sensors: %s
        """ % (self.scanserver.sensors)
        return res

    def ascan(self, name, *parameters):
        self.load(name)
        nb_parameters = len(parameters)
        if(nb_parameters > 0):
            
            #check parameters compatibilities
            req_parameters = 0;
            for d in self.dims:
                req_parameters += d * 2 + 1
            req_parameters += 1
            print "req parameters length %d" % req_parameters
            if (len(parameters) != req_parameters):
                raise PyTango.DevFailed("Scan parameters not compatibles with the current configuration")

            ideb = 0
            ifin = 1
            for i, d in enumerate(self.dims):
                trajectories = []
                
                #get the number of step for the ith dimension 
                idx = 0;
                for j in range(i+1):
                    idx += self.dims[j] * 2 + j
                n = int(parameters[idx])
                print "step (%d): %d" % (idx, n)

                for j in range(d):
                    debut = float(parameters[ideb])
                    fin = float(parameters[ifin])
                    step = (fin - debut) / n
                    trajectories += [debut + numpy.arange(n + 1) * step]
                    print "[%f, %f]" % (debut, fin)
                    ideb = ideb + 2
                    ifin = ifin + 2
                ideb = idx + 1
                ifin = idx + 2
                
                if i == 0:
                    dt = float(parameters[-1])
                    print "dt: %f" % dt
                    self.scanserver.integrationTimes = numpy.ones(n + 1) * dt                   
                    self.scanserver.trajectories = trajectories
                else:
                    self.scanserver.trajectories2 = trajectories

        self.start()

    def dscan(self, name, debut, fin, n, dt):
        self.load(name)
        att_name = self.scanserver.actuators[0]
        dev_name = att_name[:att_name.rfind('/')]
        motor = Device.Device(dev_name)
        _pos = motor.position
        self.ascan(name, _pos + debut, _pos + fin, n, dt)
        motor.position = _pos
        

    def create_trajectories(self, debut, fin, n):
        step = (float(fin) - float(debut)) / n
        trajectory = numpy.arange(n+1) * step + debut
        return trajectory

    def range_trajectory(self, *list):
        size = (len(list) - 3) / 2
        debut = float(list[0])
        fin = float(list[1])
        n = float(list[2])
        trajectory = self.create_trajectory(debut, fin, n)
        for i in range(size):
            debut = fin
            fin = float(list[3 + i*2])
            n = float(list[3 + i*2 + 1])
            trajectory = numpy.concatenate([trajectory, self.create_trajectory(debut, fin, n)[1:]])
        return trajectory

"""
def ascan(m1, deb1, fin1, n, cpt, counters, dt, afterRunActionType=2, afterRunActionSensor=0):
	dm1 = (float(fin1) - float(deb1)) / float(n)

	scan2.init()
	scan2.clean()
	scan2.timebases = [cpt.name()]

	sensors = [cpt.name() + '/counter' + repr(c) for c in counters]
	#sensors += ["ans/ca/machinestatus/current"]
	scan2.sensors = sensors

	actuators = [m1.name() + "/position"]
	scan2.actuators = actuators

	trajectories = [deb1 + numpy.arange(int(n) + 1) * dm1]
	scan2.trajectories = trajectories

	scan2.integrationTimes = numpy.ones(int(n) + 1) * float(dt)

	scan2.afterRunActionType = afterRunActionType
        scan2.afterRunActionSensor = afterRunActionSensor
        print "ascan %s %f %f %d %f" % (actuators[0], deb1, fin1, n, dt)
	scan2.start()
	try:
		while scan2.state() != PyTango.DevState.ON:
			time.sleep(.1)
	except KeyboardInterrupt:
		scan2.abort()

def dscan(motor, deb1, fin1, n, cpt, counters, dt, afterRunActionType=2, afterRunActionSensor=0):
	_pos = motor.position
	ascan(motor, _pos + deb1, _pos + fin1, n, cpt, counters, dt, afterRunActionType, afterRunActionSensor)


def a2scan(m1, deb1, fin1, m2, deb2, fin2, n, cpt, counters, dt, afterRunActionType=2, afterRunActionSensor=0):
	dm1 = (float(fin1) - float(deb1)) / float(n)
        dm2 = (float(fin2) - float(deb2)) / float(n)

	scan2.init()
	scan2.clean()
	scan2.timebases = [cpt.name()]

	sensors = [cpt.name() + '/counter' + repr(c) for c in counters]
	#sensors += ["ans/ca/machinestatus/current"]
	scan2.sensors = sensors

	actuators = [m1.name() + "/position"]
	actuators += [m2.name() + "/position"]
	scan2.actuators = actuators

	trajectories = [deb1 + numpy.arange(int(n) + 1) * dm1]
	trajectories += [deb2 + numpy.arange(int(n) + 1) * dm2]
	scan2.trajectories = trajectories

	scan2.integrationTimes = numpy.ones(int(n) + 1) * float(dt)

	scan2.afterRunActionType = afterRunActionType
        scan2.afterRunActionSensor = afterRunActionSensor
        print "a2scan %s %f %f %s %f %f %d %f" % (actuators[0], deb1, fin1, actuators[1], deb2, fin2, n, dt)
	scan2.start()
	try:
		while scan2.state() != PyTango.DevState.ON:
			time.sleep(.1)
	except KeyboardInterrupt:
		scan2.abort()

def d2scan(m1, deb1, fin1, m2, deb2, fin2, n, cpt, counters, dt, afterRunActionType=2, afterRunActionSensor=0):
    _pos1 = m1.position
    _pos2 = m2.position
    a2scan(m1, _pos1 + deb1, _pos1 + fin1, m2, _pos2 + deb2, _pos2 + fin2, n, cpt, counters, dt, afterRunActionType, afterRunActionSensor)
"""
